package com.muhardin.endy.belajar.designpattern.prototype;

import java.util.HashMap;
import java.util.Map;

public class Alamat {
    private String kota;
    private String provinsi;
    private Map<String, String> detail = new HashMap<>();

    public String getKota() {
        return kota;
    }
    public void setKota(String kota) {
        this.kota = kota;
    }
    public String getProvinsi() {
        return provinsi;
    }
    public void setProvinsi(String provinsi) {
        this.provinsi = provinsi;
    }
    public Map<String, String> getDetail() {
        return detail;
    }
    public void setDetail(Map<String, String> detail) {
        this.detail = detail;
    }
    
}
