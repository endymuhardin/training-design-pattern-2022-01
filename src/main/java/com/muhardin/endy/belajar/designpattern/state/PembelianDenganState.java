package com.muhardin.endy.belajar.designpattern.state;

import java.time.LocalDateTime;

import lombok.Data;

@Data
public class PembelianDenganState {
    private StatePembelian statePembelian = new StateMenungguPembayaran();
    private String pembeli;
    private LocalDateTime waktuTransaksi;

    public void kirimNotifikasi(){
        statePembelian.kirimNotifikasi();
    }
}
