package com.muhardin.endy.belajar.designpattern.interceptor;

import com.muhardin.endy.belajar.designpattern.command.BankingCommand;

public interface BankingInterceptor {
    public void process(BankingCommand command);
}
