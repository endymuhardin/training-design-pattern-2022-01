package com.muhardin.endy.belajar.designpattern.command;

public interface BankingCommand {
    public void run();
}
