package com.muhardin.endy.belajar.designpattern.prototype;

public class DemoPrototype {
    public static void main(String[] args) throws CloneNotSupportedException {
        Customer c1 = Customer.builder().build();
        c1.setEmail("cust001@yopmail.com");
        c1.setNama("Customer 001");
        c1.getTelepon().add("0812345678901");
        c1.getTelepon().add("0812345678902");

        Alamat a1 = new Alamat();
        a1.setKota("Bogor");
        a1.setProvinsi("Jawa Barat");
        a1.getDetail().put("Nomer Rumah", "10");
        c1.setAlamat(a1);

        Customer c2 = (Customer) c1.clone();
        c2.getAlamat().setKota("Bandung");
        c2.getAlamat().getDetail().put("Nomer Rumah", "11");
        System.out.println("C2 nama : "+c2.getNama());
        System.out.println("C2 email : "+c2.getEmail());
        for(String telepon : c2.getTelepon()){
            System.out.println("Telepon : "+telepon);
        }
        System.out.println("c1 == c2 ? "+(c1 == c2));
        System.out.println("Provinsi : "+c2.getAlamat().getProvinsi());
        System.out.println("Kota C1 : "+c1.getAlamat().getKota());
        System.out.println("Kota C2 : "+c2.getAlamat().getKota());
        System.out.println("Nomer rumah C1 : "+c1.getAlamat().getDetail().get("Nomer Rumah"));
        System.out.println("Nomer rumah C2 : "+c2.getAlamat().getDetail().get("Nomer Rumah"));
        System.out.println("c1.equals(c2) ? "+(c1.equals(c2)));
    }
}
