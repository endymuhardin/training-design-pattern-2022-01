package com.muhardin.endy.belajar.designpattern.builder;

import java.math.BigDecimal;

public class ProdukBuilder {
    private Produk hasil;

    public ProdukBuilder(){
        hasil = new Produk();
    }

    public ProdukBuilder kode(String kode) {
        hasil.setKode(kode);
        return this;
    }
    public ProdukBuilder nama(String nama) {
        hasil.setNama(nama);
        return this;
    }
    public ProdukBuilder harga(BigDecimal harga) {
        hasil.setHarga(harga);
        return this;
    }

    public Produk build(){
        if(hasil.getHarga() == null){
            throw new IllegalStateException("Harga produk tidak boleh null");
        }
        if(BigDecimal.ZERO.compareTo(hasil.getHarga()) > 0){
            throw new IllegalStateException("Harga produk tidak boleh minus");
        }
        return hasil;
    }
}
