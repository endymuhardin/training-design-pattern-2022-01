package com.muhardin.endy.belajar.designpattern.state;

public class DemoState {
    public static void main(String[] args) {
        PembelianDenganState pembelian = new PembelianDenganState();
        pembelian.kirimNotifikasi();

        // customer melakukan pembayaran
        pembelian.setStatePembelian(new StatePembayaranDiterima());
        pembelian.kirimNotifikasi();
    }
}
