package com.muhardin.endy.belajar.designpattern.facade;

import java.math.BigDecimal;

public class BankingService {

    public void setor(String nasabah, BigDecimal nilaiSetoran) {
        System.out.println("Nasabah "+nasabah+" melakukan setoran senilai "+nilaiSetoran);
    }

    public void transfer(String pengirim, String penerima, BigDecimal nilaiTransfer) {
        System.out.println("Nasabah "+pengirim+" mentransfer senilai "+nilaiTransfer+" kepada nasabah "+penerima);
    }
    
}
