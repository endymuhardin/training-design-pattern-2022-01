package com.muhardin.endy.belajar.designpattern.interceptor;

import java.math.BigDecimal;

import com.muhardin.endy.belajar.designpattern.command.BankingCommand;
import com.muhardin.endy.belajar.designpattern.command.SetoranCommand;
import com.muhardin.endy.belajar.designpattern.command.TransferCommand;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class NilaiSetoranInterceptor implements BankingInterceptor {

    private BigDecimal limitTransaksi;

    @Override
    public void process(BankingCommand command) {
        if(SetoranCommand.class.isAssignableFrom(command.getClass())) {
            SetoranCommand cmd = (SetoranCommand) command;
            cekLimitTransaksi(cmd.getNilai());
        }

        if(TransferCommand.class.isAssignableFrom(command.getClass())){
            TransferCommand cmd = (TransferCommand) command;
            cekLimitTransaksi(cmd.getNilaiTransfer());
        }
        
    }

    private void cekLimitTransaksi(BigDecimal nilai) {
        if(limitTransaksi.compareTo(nilai) < 0) {
            throw new IllegalStateException("Nilai transaksi melebihi limit");
        }
    }
    
}
