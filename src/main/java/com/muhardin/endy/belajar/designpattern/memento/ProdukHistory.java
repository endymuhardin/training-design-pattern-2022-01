package com.muhardin.endy.belajar.designpattern.memento;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;

@Getter
public class ProdukHistory {
    private List<ProdukSnapshot> daftarSnapshot = new ArrayList<>();
    public void simpan(ProdukSnapshot snapshot){
        daftarSnapshot.add(snapshot);
    }
}
