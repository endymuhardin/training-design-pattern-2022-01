package com.muhardin.endy.belajar.designpattern.command;

import java.math.BigDecimal;

public class CommandDemo {
    public static void main(String[] args) {
        BankingService service = new BankingService();

        String nasabah = "N-001";
        BigDecimal nilaiSetoran = new BigDecimal(750000);

        SetoranCommand setorCmd = new SetoranCommand(nasabah, nilaiSetoran);
        service.execute(setorCmd);

        String nasabah2 = "N-002";
        BigDecimal nilaiTransfer = new BigDecimal(1500000);
        TransferCommand transferCmd = new TransferCommand(nasabah2, nasabah, nilaiTransfer);
        service.execute(transferCmd);
    }
}
