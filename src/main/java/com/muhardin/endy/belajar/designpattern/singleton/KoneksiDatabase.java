package com.muhardin.endy.belajar.designpattern.singleton;

public class KoneksiDatabase {
    private String dbName;
    private String dbUsername;
    private String dbPassword;

    public KoneksiDatabase(String dbName, String dbUsername, String dbPassword) {
        this.dbName = dbName;
        this.dbUsername = dbUsername;
        this.dbPassword = dbPassword;
    }

    public void connect(){
        System.out.println("Membuat koneksi ke database "+dbName+" dengan username ["
        +dbUsername+"] dan password ["+dbPassword+"]");
        System.out.println("Object id : "+this.toString());
    }
}
