package com.muhardin.endy.belajar.designpattern.strategy;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import lombok.Data;

@Data
public class Pembelian {
    private LocalDateTime waktuTransaksi;
    private String namaPembeli;
    private List<PembelianDetail> daftarPembelian = new ArrayList<>();
    private List<Diskon> daftarDiskon = new ArrayList<>();

    public BigDecimal total(){
        BigDecimal hasil = BigDecimal.ZERO;

        for(PembelianDetail pd : daftarPembelian){
            hasil = hasil.add(pd.subtotal());
        }

        return hasil;
    }

    public BigDecimal totalDiskon(){
        BigDecimal hasil = BigDecimal.ZERO;
        for(Diskon d : daftarDiskon){
            BigDecimal disc = d.hitung(this);
            System.out.println(d.getClass().getSimpleName()+" sebesar "+disc.setScale(2, RoundingMode.HALF_EVEN));
            hasil = hasil.add(disc);
        }
        return hasil;
    }

    public BigDecimal dibayar(){
        return total().subtract(totalDiskon());
    }
}
