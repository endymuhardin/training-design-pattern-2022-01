package com.muhardin.endy.belajar.designpattern.singleton;

public class KoneksiDatabaseSingleton {
    private String dbName;
    private String dbUsername;
    private String dbPassword;

    private static KoneksiDatabaseSingleton kds = null;

    private KoneksiDatabaseSingleton(String dbName, String dbUsername, String dbPassword) {
        this.dbName = dbName;
        this.dbUsername = dbUsername;
        this.dbPassword = dbPassword;
    }

    public static KoneksiDatabaseSingleton getKoneksiDatabase(String dbName, String dbUsername, String dbPassword){
        if(kds == null) {
            kds = new KoneksiDatabaseSingleton(dbName, dbUsername, dbPassword);
        }
        return kds;
    }

    public void connect(){
        System.out.println("Membuat koneksi ke database "+dbName+" dengan username ["
        +dbUsername+"] dan password ["+dbPassword+"]");
        System.out.println("Object id : "+this.toString());
    }
}
