package com.muhardin.endy.belajar.designpattern.strategy;

import java.math.BigDecimal;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class DiskonTotal implements Diskon {

    private BigDecimal batasMinimalDiskon;
    private BigDecimal persentaseDiskon;

    @Override
    public BigDecimal hitung(Pembelian pembelian) {
        if(batasMinimalDiskon.compareTo(pembelian.total()) < 0){
            return persentaseDiskon.multiply(pembelian.total());
        }
        return BigDecimal.ZERO;
    }
    
}
