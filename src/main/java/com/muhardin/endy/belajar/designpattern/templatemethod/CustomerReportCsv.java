package com.muhardin.endy.belajar.designpattern.templatemethod;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Stream;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class CustomerReportCsv extends ReportGenerator {

    private String outputFile = "customer.xlsx";
    private String namaFileCustomer;
    private Boolean pakaiHeader = true;
    private List<Customer> dataCustomer = new ArrayList<>();

    public CustomerReportCsv(String namafile){
        this.namaFileCustomer = namafile;
    }

    @Override
    protected void fetchData() {
        System.out.println("Mengambil data");
        try (Stream<String> stream = Files.lines(Paths.get(this.getClass().getClassLoader()
                .getResource(namaFileCustomer).toURI()))) {
            AtomicInteger baris = new AtomicInteger(0);
			stream.forEach(data -> {
                if(baris.incrementAndGet() == 1 && pakaiHeader) {
                    return;
                }
                String[] rowData = data.split(",");
                Customer c = new Customer();
                c.setId(rowData[0]);
                c.setKode(rowData[1]);
                c.setNama(rowData[2]);
                c.setEmail(rowData[3]);
                dataCustomer.add(c);
            });

		} catch (Exception e) {
			e.printStackTrace();
		}

        System.out.println("Jumlah data : "+dataCustomer.size());
    }

    @Override
    protected void validate() {
        System.out.println("Melakukan validasi");
    }

    @Override
    protected void render() {
        System.out.println("Generate report");
        Workbook workbook = new XSSFWorkbook();

        Sheet sheet = workbook.createSheet("Customer");
        sheet.setColumnWidth(0, 6000);
        sheet.setColumnWidth(1, 8000);
        sheet.setColumnWidth(2, 8000);

        Row header = sheet.createRow(0);

        CellStyle headerStyle = workbook.createCellStyle();
        headerStyle.setFillForegroundColor(IndexedColors.LIGHT_BLUE.getIndex());
        headerStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);

        XSSFFont font = ((XSSFWorkbook) workbook).createFont();
        font.setFontName("Arial");
        font.setFontHeightInPoints((short) 16);
        font.setBold(true);
        headerStyle.setFont(font);

        Cell headerCell = header.createCell(0);
        headerCell.setCellValue("Kode");
        headerCell.setCellStyle(headerStyle);
        headerCell = header.createCell(1);
        headerCell.setCellValue("Nama");
        headerCell.setCellStyle(headerStyle);
        headerCell = header.createCell(2);
        headerCell.setCellValue("Email");
        headerCell.setCellStyle(headerStyle);

        CellStyle style = workbook.createCellStyle();
        style.setWrapText(true);

        AtomicInteger baris = new AtomicInteger(0);
        for(Customer c : dataCustomer){
            Row row = sheet.createRow(baris.incrementAndGet());
            Cell cell = row.createCell(0);
            cell.setCellValue(c.getKode());
            cell.setCellStyle(style);
    
            cell = row.createCell(1);
            cell.setCellValue(c.getNama());
            cell.setCellStyle(style);

            cell = row.createCell(2);
            cell.setCellValue(c.getEmail());
            cell.setCellStyle(style);
        }

        File currDir = new File(".");
        String path = currDir.getAbsolutePath() + File.separator + "target" + File.separator;
        String fileLocation = path + outputFile;

        try (FileOutputStream outputStream = new FileOutputStream(fileLocation)) {
            workbook.write(outputStream);
            workbook.close();
        } catch (Exception err) {
            err.printStackTrace();
        }
        
        
        
    }
    
}
