package com.muhardin.endy.belajar.designpattern.memento;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class DemoMemento {
    public static void main(String[] args) {
        Produk p = new Produk();
        p.setKode("P-001");
        p.setNama("Produk 001");
        p.setFoto("p001.jpg");
        p.setHarga(new BigDecimal(100000));

        ProdukHistory history = new ProdukHistory();
        history.simpan(p.saveSnapshot());

        p.setFoto("p0011.jpg");
        p.setHarga(new BigDecimal(120000));
        history.simpan(p.saveSnapshot());

        displayProduk(p);
        p.restoreSnapshot(history.getDaftarSnapshot().get(0));
        displayProduk(p);
        
    }

    private static void displayProduk(Produk p) {
        System.out.println("Kode Produk : "+p.getKode());
        System.out.println("Nama Produk : "+p.getNama());
        System.out.println("Foto Produk : "+p.getFoto());
        System.out.println("Harga Produk : "+p.getHarga().setScale(0, RoundingMode.HALF_EVEN));
    }
}
