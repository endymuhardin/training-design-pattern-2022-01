package com.muhardin.endy.belajar.designpattern.strategy;

import java.math.BigDecimal;

import lombok.Data;

@Data
public class PembelianDetail {

    private Produk produk;
    private Integer jumlah;

    public BigDecimal subtotal() {
        return produk.getHarga().multiply(new BigDecimal(jumlah));
    }
    
}
