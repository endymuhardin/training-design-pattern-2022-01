package com.muhardin.endy.belajar.designpattern.facade;

import java.math.BigDecimal;

public class FacadeDemo {
    public static void main(String[] args) {
        String nasabah = "N-001";
        BigDecimal nilaiSetoran = new BigDecimal(750000);

        BankingService bankingService = new BankingService();
        bankingService.setor(nasabah, nilaiSetoran);

        String nasabah2 = "N-002";
        BigDecimal nilaiTransfer = new BigDecimal(1500000);
        bankingService.transfer(nasabah2, nasabah, nilaiTransfer);
        
    }
}
