package com.muhardin.endy.belajar.designpattern.state;

public enum StatusPembelian {
    MENUNGGU_PEMBAYARAN,
    PEMBAYARAN_DITERIMA,
    DIPROSES,
    DIKIRIM,
    DITERIMA,
    SELESAI,
    KOMPLAIN
}
