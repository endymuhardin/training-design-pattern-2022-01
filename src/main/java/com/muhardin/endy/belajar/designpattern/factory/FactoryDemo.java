package com.muhardin.endy.belajar.designpattern.factory;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

import com.muhardin.endy.belajar.designpattern.strategy.Diskon;
import com.muhardin.endy.belajar.designpattern.strategy.Pembelian;
import com.muhardin.endy.belajar.designpattern.strategy.PembelianDetail;
import com.muhardin.endy.belajar.designpattern.strategy.Produk;

public class FactoryDemo {
    public static void main(String[] args) {
        Produk p001 = new Produk();
        p001.setKode("M-001");
        p001.setNama("Mouse Logitech");
        p001.setHarga(new BigDecimal(500000));

        Produk p002 = new Produk();
        p002.setKode("L-001");
        p002.setNama("Laptop Lenovo");
        p002.setHarga(new BigDecimal(15000000));

        PembelianDetail pd1 = new PembelianDetail();
        pd1.setProduk(p001);
        pd1.setJumlah(2);

        PembelianDetail pd2 = new PembelianDetail();
        pd2.setProduk(p002);
        pd2.setJumlah(1);

        Pembelian pembelian = new Pembelian();
        pembelian.setNamaPembeli("Agus");
        pembelian.setWaktuTransaksi(LocalDateTime.now());
        pembelian.getDaftarPembelian().add(pd1);
        pembelian.getDaftarPembelian().add(pd2);

        // mengaktifkan diskon
        List<Diskon> daftarDiskon = DiskonFactory.builder()
                            .enableDiskonProduk(true)
                            .daftarProdukDiskon(Arrays.asList("Logitech", "Lenovo"))
                            .persentaseDiskonProduk(new BigDecimal(0.15))
                            //.enableDiskonTotal(true)
                            .batasDiskonTotal(new BigDecimal(15000000))
                            .persentaseDiskonTotal(new BigDecimal(0.2))
                            .build().getActiveDiskon();
        
        pembelian.setDaftarDiskon(daftarDiskon);

        System.out.println("Total pembelian : "+pembelian.total().setScale(2, RoundingMode.HALF_EVEN));
        System.out.println("Total diskon : "+pembelian.totalDiskon().setScale(2, RoundingMode.HALF_EVEN));
        System.out.println("Yang harus dibayar : "+pembelian.dibayar().setScale(2, RoundingMode.HALF_EVEN));
    }
}
