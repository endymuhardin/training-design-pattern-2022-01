package com.muhardin.endy.belajar.designpattern.command;

public class BankingService {
    public void execute(BankingCommand cmd) {
        cmd.run();
    }
}
