package com.muhardin.endy.belajar.designpattern.strategy;

import java.math.BigDecimal;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class DiskonProduk implements Diskon {

    private String namaProduk;
    private BigDecimal persentaseDiskon;

    @Override
    public BigDecimal hitung(Pembelian pembelian) {
        BigDecimal hasil = BigDecimal.ZERO;
        for(PembelianDetail pd : pembelian.getDaftarPembelian()){
            if(pd.getProduk().getNama().toLowerCase().contains(namaProduk.toLowerCase())) {
                hasil = hasil.add(persentaseDiskon.multiply(pd.subtotal()));
            }
        }
        return hasil;
    }
    
}
