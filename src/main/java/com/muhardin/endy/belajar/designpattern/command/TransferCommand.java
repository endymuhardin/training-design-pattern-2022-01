package com.muhardin.endy.belajar.designpattern.command;

import java.math.BigDecimal;

import lombok.Getter;

@Getter
public class TransferCommand implements BankingCommand {
    private String pengirim;
    private String penerima;
    private BigDecimal nilaiTransfer;

    public TransferCommand(String pengirim, String penerima, BigDecimal nilaiTransfer) {
        this.pengirim = pengirim;
        this.penerima = penerima;
        this.nilaiTransfer = nilaiTransfer;
    }

    @Override
    public void run() {
        System.out.println("Nasabah "+pengirim+" mentransfer senilai "+nilaiTransfer+" kepada nasabah "+penerima);
    }

}
