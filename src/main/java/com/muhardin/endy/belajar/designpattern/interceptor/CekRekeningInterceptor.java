package com.muhardin.endy.belajar.designpattern.interceptor;

import com.muhardin.endy.belajar.designpattern.command.BankingCommand;
import com.muhardin.endy.belajar.designpattern.command.SetoranCommand;

public class CekRekeningInterceptor implements BankingInterceptor {

    @Override
    public void process(BankingCommand command) {
        if(SetoranCommand.class.isAssignableFrom(command.getClass())){
            SetoranCommand cmd = (SetoranCommand) command;
            if("Nasabah 99".equals(cmd.getNasabah())){
                throw new IllegalArgumentException("Nasabah 99 rekeningnya diblokir");
            }
        }
    }
    
}
